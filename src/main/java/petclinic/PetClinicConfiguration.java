package petclinic;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PetClinicConfiguration {

	@Autowired
	private PetClinicProperties petClinicProp;
	
	@PostConstruct
	public void init() {
		System.out.println("display "+petClinicProp.isDisplayOwnersWithPets());
	}
	
}
