package petclinic.dao.mem;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import petclinic.dao.OwnerRepository;
import petclinic.model.Owner;

@Repository
public class OwnerRepositoryInMemImp implements OwnerRepository {

	private Map<Long,Owner> ownersMap=new HashMap<>();

	public OwnerRepositoryInMemImp() {
		
		Owner owner1=new Owner();
		owner1.setId(1L);
		owner1.setFirstName("Murat");
		owner1.setLastName("Kırmızıgül");
		
		Owner owner2=new Owner();
		owner2.setId(2L);
		owner2.setFirstName("Ayaz");
		owner2.setLastName("Kırmızıgül");
		
		Owner owner3=new Owner();
		owner3.setId(3L);
		owner3.setFirstName("Mehmet");
		owner3.setLastName("korkmaz");
		
		Owner owner4=new Owner();
		owner4.setId(4L);
		owner4.setFirstName("Birol");
		owner4.setLastName("Tercan");
		
		ownersMap.put(owner1.getId(), owner1);
		ownersMap.put(owner2.getId(), owner2);
		ownersMap.put(owner3.getId(), owner3);
		ownersMap.put(owner4.getId(), owner4);
	}

	@Override
	public List<Owner> findAll() {
		
		return new ArrayList<>(this.ownersMap.values());
	}

	@Override
	public Owner findById(Long id) {
		// TODO Auto-generated method stub
		return ownersMap.get(id);
	}

	@Override
	public List<Owner> findOwnerByLastName(String lastName) {
		return ownersMap.values().stream().filter(o->o.getLastName().equals(lastName)).collect(Collectors.toList());
	}

	@Override
	public void create(Owner owner) {
		owner.setId(new Date().getTime());
		ownersMap.put(owner.getId(), owner);
	}

	@Override
	public Owner update(Owner owner) {
		// TODO Auto-generated method stub
		return ownersMap.replace(owner.getId(), owner);
	}

	@Override
	public void delete(Long id) {

		ownersMap.remove(id);

	}

}
