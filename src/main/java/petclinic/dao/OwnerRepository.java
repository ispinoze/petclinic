package petclinic.dao;

import java.util.List;

import petclinic.model.Owner;

public interface OwnerRepository {

	List<Owner> findAll();
	
	Owner findById(Long id);
	List<Owner> findOwnerByLastName(String lastName);
	void create(Owner owner);
	Owner update(Owner owner);
	void delete(Long id);
}
