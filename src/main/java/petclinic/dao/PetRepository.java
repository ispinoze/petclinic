package petclinic.dao;

import java.util.List;

import petclinic.model.Owner;
import petclinic.model.Pet;

public interface PetRepository {
	List<Pet> findAll();
	Pet findById(Long id);
	List<Pet> findByOwnerId(Long ownerId);
	void create(Pet pet);
	Owner update(Pet pet);
	void delete(Long id);
	void deleteByOwnerId(Long ownerId);
}
