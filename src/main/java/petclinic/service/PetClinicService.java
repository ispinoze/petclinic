package petclinic.service;

import java.util.List;

import petclinic.exception.OwnerNotFoundException;
import petclinic.model.Owner;

public interface PetClinicService {
	
	List<Owner> findOwners();
	
	List<Owner> findOwnersByLastName(String lastName);
	
	Owner findOwner(Long id) throws OwnerNotFoundException;

	void createOwner(Owner owner);
	
	void updateOwner(Owner owner);
	
	void deleteOwner(Long id);
}
