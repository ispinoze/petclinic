package petclinic.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import petclinic.dao.OwnerRepository;
import petclinic.exception.OwnerNotFoundException;
import petclinic.model.Owner;

@Service
public class PetClinicImp implements PetClinicService {

	private OwnerRepository ownerRepository;
	
	@Autowired
	public void setOwnerRepository(OwnerRepository ownerRepository) {
		this.ownerRepository = ownerRepository;
	}

	@Override
	public List<Owner> findOwners() {
		// TODO Auto-generated method stub
		return this.ownerRepository.findAll();
	}

	@Override
	public List<Owner> findOwnersByLastName(String lastName) {
		// TODO Auto-generated method stub
		return this.ownerRepository.findOwnerByLastName(lastName);
	}

	@Override
	public Owner findOwner(Long id) throws OwnerNotFoundException {
		// TODO Auto-generated method stub
		Owner owner=this.ownerRepository.findById(id);
		if(owner==null) throw new OwnerNotFoundException("Owner not found with id: "+id);
		return owner;
	}

	@Override
	public void createOwner(Owner owner) {
		// TODO Auto-generated method stub
		this.ownerRepository.create(owner);
	}

	@Override
	public void updateOwner(Owner owner) {
		// TODO Auto-generated method stub
		this.ownerRepository.update(owner);
	}

	@Override
	public void deleteOwner(Long id) {
		// TODO Auto-generated method stub
		this.ownerRepository.delete(id);
	}

}
