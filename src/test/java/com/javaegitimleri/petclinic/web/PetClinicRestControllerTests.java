package com.javaegitimleri.petclinic.web;


import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import petclinic.model.Owner;

public class PetClinicRestControllerTests {

	private RestTemplate restTemplate;
	
	@Before
	public void setUp() {
		
		restTemplate=new RestTemplate();
	}
	
	@Test
	public void testCreateOwner() {
		Owner owner=new Owner();
		owner.setFirstName("Metehan");
		owner.setLastName("Yücel");
		
		URI location=this.restTemplate.postForLocation("http://localhost:8080/rest/owner", owner);
		
		Owner owner2=this.restTemplate.getForObject(location, Owner.class);
		
		MatcherAssert.assertThat(owner2.getFirstName(), Matchers.equalTo(owner.getFirstName()));
		MatcherAssert.assertThat(owner2.getLastName(), Matchers.equalTo(owner.getLastName()));
		
		this.restTemplate.delete(location);
		
	}
	
	@Test
	public void testGetOwnerById() {
		
		ResponseEntity<Owner> resp=restTemplate.getForEntity("http://localhost:8080/rest/owner/1", Owner.class);
		
		MatcherAssert.assertThat(resp.getStatusCodeValue(), Matchers.equalTo(200));
		
		MatcherAssert.assertThat(resp.getBody().getFirstName(), Matchers.equalTo("Murat"));
	}
	
	@Test
	public void testGetOwnerByLastname() {
		
		ResponseEntity<List> resp=restTemplate.getForEntity("http://localhost:8080/rest/owner?ln=Kırmızıgül", List.class);
		
		MatcherAssert.assertThat(resp.getStatusCodeValue(), Matchers.equalTo(200));
		//MatcherAssert.assertThat(resp.getBody().);
		List<Map<String,String>> body=resp.getBody();
		
		List<String> firstNames=body.stream().map(e->e.get("firstName")).collect(Collectors.toList());
		
		MatcherAssert.assertThat(firstNames, Matchers.containsInAnyOrder("Murat","Ayaz"));
	}
	
	@Test
	public void testGetOwners() {
		ResponseEntity<List> resp=restTemplate.getForEntity("http://localhost:8080/rest/owners", List.class);
		
		MatcherAssert.assertThat(resp.getStatusCodeValue(), Matchers.equalTo(200));
		//MatcherAssert.assertThat(resp.getBody().);
		List<Map<String,String>> body=resp.getBody();
		
		List<String> firstNames=body.stream().map(e->e.get("firstName")).collect(Collectors.toList());
		
		MatcherAssert.assertThat(firstNames, Matchers.contains("Murat","Ayaz","Mehmet","Birol"));
	}
	
	
}
